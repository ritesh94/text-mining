# -*- coding: utf-8 -*-

import os
import pandas as pd
import nltk
import csv
import re
global collections
import collections

##os.chdir(r'C:\Users\ritesh\Desktop\TextMining\Project Files')
#path=r'C:\Users\ritesh\Desktop\TextMining\Project Files' 
#mode=777
#os.chmod(path, mode)

file = 'data\\osha.xlsx'
datafile = pd.read_excel(file,header=None,names=["ID","cause","summary","summary1","summary2"])
summary = datafile["summary"]
#test_data = datafile

body_parts_list=["abdomen","Adam's apple","adenoids","adrenal gland","anatomy","ankle","anus","appendix","arch","arm","artery","back","ball of the foot","belly","belly button","big toe","bladder","blood","blood vessels","body","bone","brain","breast","buttocks","calf","capillary","carpal","cartilage","cell","cervical vertebrae","cheek","chest","chin","circulatory system","clavicle","coccyx","collar bone","diaphragm","digestive system","ear","ear lobe","elbow","endocrine system","esophagus","eye","eyebrow","eyelashes","eyelid","face","fallopian tubes","feet","femur","fibula","filling","finger","fingernail","follicle","foot","forehead","gallbladder","glands","groin","gums","hair","hand","head","heart","heel","hip","humerus","immune system","instep","index finger","intestines","iris","jaw","kidney","knee","larynx","leg","ligament","lip","liver","lobe","lumbar vertebrae","lungs","lymph node","mandible","metacarpal","metatarsal","molar","mouth","muscle","nail","navel","neck","nerves","nipple","nose","nostril","organs","ovary","palm","pancreas","patella","pelvis","phalanges","pharynx","pinky","pituitary","pore","pupil","radius","rectum","red blood cells","respiratory system","ribs","sacrum","scalp","scapula","senses","shin","shoulder","shoulder blade","skeleton","skin","skull","sole","spinal column","spinal cord","spine","spleen","sternum","stomach","tarsal","teeth","tendon","testes","thigh","thorax","throat","thumb","thyroid","tibia","tissue","toe","toenail","tongue","tonsils","tooth","torso","trachea","ulna","ureter","urethra","urinary system","uterus","uvula","vein","vertebra","waist","white blood cells","wrist"]
regx = re.compile('[ ,;:!?.:]')


def body_parts(datafile):
    final_list=[]
    for index,row in datafile.iterrows():
        text=row[2]
        #print(text)
        #print(set(regx.split(text)) & set(body_parts_list))
        matches = set(regx.split(text)) & set(body_parts_list)
        for match in matches:
            final_list.append(match)
    return final_list    


body_parts_match=body_parts(datafile)
counts = collections.Counter(body_parts_match)
print (counts)



j=""
for i in body_parts_match:
    if(i!='no_occupation'):
        j=j+" "+i
print (j)

# Let's plot the result
import wordcloud
from wordcloud import WordCloud, ImageColorGenerator
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image


wc = WordCloud(background_color="white").generate(" ".join(body_parts_match))
plt.imshow(wc, interpolation='bilinear')
plt.axis("off")
plt.show()
fig = plt.gcf()
fig.set_size_inches(18.5, 10.5)
fig.savefig('wordcloud_body_parts.png', dpi=100)



body_parts_match=[[x] for x in body_parts_match]
with open("Body_Parts.csv","w") as f:
        wr=csv.writer(f,quoting=csv.QUOTE_ALL)
        wr.writerow(['Body Parts'])
        wr.writerows(body_parts_match)

